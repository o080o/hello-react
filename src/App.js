import logo from './logo.svg';
import './App.css';
import { useState, useEffect } from 'react'
import axios from 'axios'
import Button from '@mui/joy/Button';
import ParulButton from './ParulButton';

// Add more state. Click button name change + show "<name> - loading gender" -> "<name> - female"

function useResponseState(name) {
  // what does this do? -- This should update the internal state of some data
  // make at least one api call to something


  // look at this cool "destructuring" syntax!! useState returns an array, with exactly two elements. we bind them to variables named "state", and "setState"
  // functions are "first class objects" in javascript!!! they are just objects, that can be bound to variables, and passed as arguments.
  var [state, setState] = useState({
    count: -1,
    gender: "gender-default",
    probability: 0.2
  }) // we need to setup a place in the state for us to use, and give it a default

  // I refactored this out of the useEffect hook, so it is more readable. I forgot the actual function of a useEffect hook can't be async (hence the warning on the console)
  async function fetchData() {
    console.log('In the async fetchData function!');
    setState({gender:"Loading..."})
    const responseData = await axios.get("https:api.genderize.io?name="+name);
    console.log('this is what I got from the response:', responseData.data.count - responseData.data.gender, ' for input ', name);
    // {"count":1011867,"name":"mary","gender":"female","probability":1.0}
    setState(responseData.data);
  }

  console.log('Im in the useResponseState hook!');
//for side effects 
  useEffect(
    () => {

      console.log('Im in the useeffect hook ! ', name);
      // we call the async function here, and do *not* wait for it (because we do not have access to 'await', since this isn't an async function)
      
      fetchData();
    },
    [name] //TODO: what is this empty array for? If name changes only then fire useEffect 
  );
  /* Alex's notes
     useEffect(async () => { // note the "async" keyword here! this is the only place so far that we can use this keyword!!
        var weatherData = await axios.get("https://weather.com/weather/today/l/32.7874,-96.7989"); // "await" is the magic word!
        setState(weatherData)
     },
      []);
  */
  // what should we return here? don't want to hardcode.. use state.something here?
  // how do we know when the request is done
  return {
    //status: "pending", // what do we do here??
    //weather:  state.somethinfgInTheJsonPayload || "unknown", // look at this cool short-ciscuiting boolean operator! js uses this all the time!
    count: state.count || 9,
    gender: state.gender || "gender-default",
    probability: state.probability || 0.0
  }
}

function App() {
  
  console.log('I am in the render function!');
  var [name, setName] = useState("Vihaan");  
  

  var responseState = useResponseState(name);
  // TODO I want a way to change the name! maybe randomize from a small list? or even just start by changing from hardcoded 'parul' to hardcoded 'alex'. I don't want to get into the hassle of having a text-field input yet though...
  async function changeName() {
    // ?? how can we react to user input?
    // this functino get's called when the button is clicked, but how can we "alter" the state from here?
   
    if(Math.random()>0.5) {
      setName('Parul');
     
    } else {
      setName('Alex');
    }
    console.log('randomize button clicked...')
  }
  // show
  if (responseState.count === -1) {
    return (
      <div className="App">
        <header className="App-header">
          loading, please wait
        </header>
      </div>
    );
  } else {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            The response is: {name} - {responseState.gender}.

            Edit <code>src/App.js</code> and save to reload.

            Did it!!
          </p>
          {
            /* TODO note, this is a comment inside of a pair of {}!!
             * since this block is jsx, the // syntax doesn't work!
             * to add comments in jsx blocks, you need to either escape into js syntax using {}, or use html comments which are 100% worse (<!-->)
             *
             * Also, note that we set the value of a property, "onClick", to the value of the variable "changeName". We did not *call* the changeName function, we pass it as a value.
             * This is how react does callbacks. And, we can give it an 'async' function without any concern in callbacks. (We can go over in more detail, but all 'async' functions are really just syntax sugar
             * around regular functions. react + useEFfect is a special case where there are linting and compiler warnings to prevent you from using 'async' functions in certain places and helping you avoid some dumb mistakes... )
             * */
          }
          
          <ParulButton onClick={changeName} name="parul" />
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
      </div>
    );
  }
}

export default App;
